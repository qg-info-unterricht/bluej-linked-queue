
/**
 * Eine Queue basierend auf einer verketteten Liste
 * 
 * @author Rainer Helfrich
 * @version Oktober 2020
 */
public class LinkedQueue<T> extends Queue<T>
{
    /**
     * Erzeugt eine neue, leere Queue
     */
    public LinkedQueue()
    {

    }
    
    /**
     * Gibt das vorderste Element der Queue zurück (falls sie nicht leer ist)
     * @return Das vorderste Element
     */
    public T front()
    {
        return null;
    }
    
    /**
     * Entfernt das vorderste Element aus der Queue (falls sie nicht leer ist) und gibt es zurück
     * @return Das bisherige vorderste Element
     */
    public T dequeue()
    {
        return null;
    }
    
    /**
     * Fügt ein neues Element hinten in der Schlange ein
     * @param x Das neue Element
     */
    public void enqueue(T x)
    {
    }
    
    /**
     * Gibt zurück, ob die Queue leer ist
     * @return true, wenn die Queue leer ist; false sonst
     */
    public boolean isEmpty()
    {
        return false;
    }
}
